package pages.page;


import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Button;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.DropDown;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.TextInput;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.tables.Cell;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.tables.RowBasedTable;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Lazy;
import pages.CommonPage;

/**
 * Created by pchelintsev on 5/23/2016.
 */
public class AdditionalInformationPage extends CommonPage {

    @Lazy
    @FindBy(xpath = ".//tr[td//span[contains(text(),'test mobile')]]//input")
    private TextInput testMobileTextInput;

    @Lazy
    @FindBy(xpath = ".//label[contains(text(),'Yes Domestic')]")
    private Button buttonYesDomestic;

    @Lazy
    @FindBy(xpath = ".//tr[td[span[contains(text(),'Worker requires')]]]//select")
    private DropDown workerRequiresAccessDropDown;

    @Lazy
    @FindBy(xpath = ".//tr[td[span[contains(text(),'Medical Exam Required')]]]//select")
    private DropDown medicalExamRequiredDropDown;

    public void enterTextToMobileTextInput(String text){
        try {
            testMobileTextInput.enterText(text);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Lazy
    @FindBy(xpath = ".//div[h4[contains(text(),'Custom')]]//table/tbody")
    private Button container_Table;

    public RowBasedTable<Cell> inputTable() throws Exception {
        RowBasedTable<Cell> table = new RowBasedTable<>(Cell.class, container_Table);
        //Xpath locator for row is set here
        table.setRowsLocator(By.xpath(".//tr[td[@class='cell2 ng-scope']//div//input]"));
        //Xpath locator for column name is set here
        table.setColumnsNameLocator(By.xpath(".//span[@class='body12bold ng-binding']"));
        //Xpath locator for cell is set here
        table.setCellLocator(By.xpath(".//input"));
        return table;
    }

    public RowBasedTable<Cell> dropdownTable() throws Exception {
        RowBasedTable<Cell> table = new RowBasedTable<>(Cell.class, container_Table);
        //Xpath locator for row is set here
        table.setRowsLocator(By.xpath(".//tr[td[@class='cell2 ng-scope']//div//select]"));
        //Xpath locator for column name is set here
        table.setColumnsNameLocator(By.xpath(".//span[@class='body12bold ng-binding']"));
        //Xpath locator for cell is set here
        table.setCellLocator(By.xpath(".//select"));
        return table;
    }

    public void clickOnButton(String buttonName,String sectionName) throws Exception {
        switch (buttonName) {
            case "No": {
                inputTable().takeCell(sectionName,0).containerMain.click();
                break;
            }
            case "Yes Domestic": {
                inputTable().takeCell(sectionName,1).containerMain.click();
                break;
            }
            case "Yes International": {
                inputTable().takeCell(sectionName,2).containerMain.click();
                break;
            }
            case "Yes both International and Domestic": {
                inputTable().takeCell(sectionName,4).containerMain.click();
                break;
            }
            case "Not Applicable": {
                inputTable().takeCell(sectionName,1).containerMain.click();
                break;
            }
            case "Yes": {
                inputTable().takeCell(sectionName,2).containerMain.click();
                break;
            }
            default: {
                throw new Exception("No such request type");
            }
        }
    }

    public void selectFromDropDown(String value,String dropDownName) throws Exception {
        dropdownTable().takeCell(dropDownName,0).containerMain.sendKeys(value);
    }





}
