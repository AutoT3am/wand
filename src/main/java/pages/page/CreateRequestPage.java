package pages.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Lazy;
import pages.CommonPage;

/**
 * Created by beckmambetov on 5/20/2016.
 */
public class CreateRequestPage extends CommonPage {

    @Lazy
    @FindBy(xpath = ".//select[@class='form-control m-b floatleft ng-valid ng-touched ng-dirty ng-valid-parse' or @class='form-control m-b floatleft ng-pristine ng-valid ng-touched' or @class='form-control m-b floatleft ng-pristine ng-untouched ng-valid']")
    private WebElement dropdownManager;

    @Lazy
    @FindBy(xpath = ".//h3[contains(text(),'Staffing')]")
    private WebElement buttonStaffing;

    @Lazy
    @FindBy(xpath = ".//h3[contains(text(),'Payroll')]")
    private WebElement buttonPayroll;

    public int getNumberOfManagers() {
        return dropdownManager.findElements(By.xpath(("./option"))).size();
    }

    public void selectFromManagerDropdown(String value) {
        dropdownManager.click();
        dropdownManager.findElement(By.xpath("./option[text()='" + value + "']")).click();
    }

    public void selectRequestType(String type) throws Exception {
        switch (type){
            case "Staffing": {
                buttonStaffing.click();
                break;
            }
            case "Payroll": {
                buttonPayroll.click();
                break;
            }
            default:{
                throw new Exception("No such request type");
            }
        }
    }
}
