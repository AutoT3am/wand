package pages.page;

import cucumber.runtime.Timeout;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.context.annotation.Lazy;
import pages.CommonPage;
import pages.base.BasePage;

import java.util.concurrent.TimeoutException;

/**
 * Created by beckmambetov on 5/24/2016.
 */
public class DetailsPage extends CommonPage {

    @Lazy
    @FindBy(xpath = ".//input[@id='startDateInput']")
    public WebElement textInputStartDate;

    @Lazy
    @FindBy(xpath = ".//input[@id='endDateInput']")
    public WebElement textInputEndDate;

    @Lazy
    @FindBy(xpath = ".//input[@id='positionsInput']")
    public WebElement textInputPositions;

    @Lazy
    @FindBy(xpath = ".//input[@ng-model='staffingReqModel.hoursPerWeek']")
    public WebElement textInputHoursPerWeek;

    @Lazy
    @FindBy(xpath = ".//input[@id='billRateInput']")
    public WebElement textInputBillRate;

    @Lazy
    @FindBy(xpath = ".//input[@ng-model='staffingReqModel.financialCustomFields[$cfIterator2].value']")
    public WebElement textInputGLCode;

    @Lazy
    @FindBy(xpath = ".//select[@id='reasonDropdown']")
    private WebElement dropdownReason;

    @Lazy
    @FindBy(xpath = ".//a[text()='Search All']")
    private WebElement linkSearchAll;

    @Lazy
    @FindBy(xpath = ".//iframe")
    private WebElement frameSearchDepartment;

    @Lazy
    @FindBy(xpath = ".//input[@id='deptName']")
    private WebElement textInputDepartmentName;

    @Lazy
    @FindBy(xpath = ".//input[@id='parentName']")
    private WebElement textInputParentName;

    @Lazy
    @FindBy(xpath = ".//input[@id='deptNum']")
    private WebElement textInputDepartmentNumber;

    @Lazy
    @FindBy(xpath = ".//button[text()='Search']")
    private WebElement buttonSearch;

    @Lazy
    @FindBy(xpath = ".//a[text()='Select']")
    private WebElement linkSelect;

    public void selectFromReasonDropdown(String value) {
        dropdownReason.click();
        dropdownReason.findElement(By.xpath(".//option[text()='" + value + "']")).click();
    }

    public void selectDepartment(String departmentName, String departmentCode, String parentName) throws TimeoutException {
        linkSearchAll.click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        textInputDepartmentName.sendKeys(departmentName);
        textInputDepartmentNumber.sendKeys(departmentCode);
        textInputParentName.sendKeys(parentName);
        buttonSearch.click();
        int counter = 0;
        while (true) {
            try {
                linkSelect.click();
                break;
            } catch (Throwable ex) {
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                counter++;
                if (counter == 8) {
                    throw new TimeoutException("Loading is too long");
                }
            }
        }

    }
}
