package pages.page;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Lazy;
import pages.CommonPage;
import pages.base.BasePage;

import javax.smartcardio.CommandAPDU;

/**
 * Created by beckmambetov on 5/23/2016.
 */
public class JobDescriptionPage extends CommonPage {

    @Lazy
    @FindBy(xpath = ".//input[@class='form-control textbox m-t-sm ng-valid ng-scope ng-dirty ng-touched ng-valid-editable' or @class='form-control textbox m-t-sm ng-pristine ng-untouched ng-valid ng-scope']")
    private WebElement textInputJobTitle;

    @Lazy
    @FindBy(xpath = ".//input[@class='form-control textbox m-b-sm m-t-sm ng-pristine ng-valid ng-touched' or @class='form-control textbox m-b-sm m-t-sm ng-pristine ng-untouched ng-valid']")
    private WebElement textInputLocation;

    @Lazy
    @FindBy(xpath = ".//div/input[@value='PT']")
    private WebElement radioButtonPartTime;

    @Lazy
    @FindBy(xpath = ".//div/input[@value='FT']")
    private WebElement radioButtonFullTime;

    @Lazy
    @FindBy(xpath = ".//a[text()='Edit']")
    private WebElement linkEdit;

    @Lazy
    @FindBy(xpath = ".//iframe[@id='dutiesEditArea_ifr']")
    private WebElement frameDuties;

    @Lazy
    @FindBy(xpath = ".//p")
    private WebElement textInputDuties;

    @Lazy
    @FindBy(xpath = ".//iframe[@id='skillsEditArea_ifr']")
    private WebElement frameSkills;

    @Lazy
    @FindBy(xpath = ".//p")
    private WebElement textInputSkills;

    @Lazy
    @FindBy(xpath = ".//iframe[@id='educationEditArea_ifr']")
    private WebElement frameEducation;

    @Lazy
    @FindBy(xpath = ".//p")
    private WebElement textInputEducation;

    public void selectJob(String name) {
        textInputJobTitle.sendKeys(name);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath(".//strong[text()='" + name + "']")).click();
    }

    public void selectLocation(String name) {
        textInputLocation.sendKeys(name);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath(".//strong[text()='" + name + "']")).click();
    }

    public void selectJobTime(String type) throws Exception {
        switch (type) {
            case "Full time": {
                radioButtonFullTime.click();
                break;
            }
            case "Part time": {
                radioButtonPartTime.click();
                break;
            }
            default: {
                throw new Exception("No such type");
            }
        }
    }

    public void openBoxesForEditing() {
        linkEdit.click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void enterTextIntoBox(String box, String text) throws Exception {
        WebElement frame,textbox;
        switch (box.toLowerCase()) {
            case "duties":
            {
                frame = frameDuties;
                textbox = textInputDuties;
                break;
            }
            case "skills":
            {
                frame = frameSkills;
                textbox = textInputSkills;
                break;
            }
            case "education":
            {
                frame = frameEducation;
                textbox = textInputEducation;
                break;
            }
            default:{
                throw new Exception("No such field");
            }
        }
        driver.switchTo().frame(frame);
        try {
            WebElement element = driver.findElement(By.xpath(".//p"));
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            textbox.click();
            Actions temp = new Actions(driver);
            temp.sendKeys(text).perform();
        } finally {
            driver.switchTo().parentFrame();
        }
    }

}
