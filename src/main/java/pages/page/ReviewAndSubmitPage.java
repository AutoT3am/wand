package pages.page;

import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Button;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Lazy;
import pages.CommonPage;

/**
 * Created by pchelintsev on 5/23/2016.
 */
public class ReviewAndSubmitPage extends CommonPage {

    @Lazy
    @FindBy(xpath = ".//button[contains(text(),'Submit')]")
    private Button submitButton;


    public void clickOnSubmitButton(){
        try {
            submitButton.click();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
