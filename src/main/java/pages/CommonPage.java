package pages;

import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Button;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.base.BasePage;

/**
 * Created by beckmambetov on 5/20/2016.
 */
public class CommonPage extends BasePage{

    @FindBy(xpath = ".//span[text()='Create Request']")
    public WebElement buttonCreateRequest;

    @FindBy(xpath = ".//button[contains(text(),'Next')]")
    public Button buttonNext;


    @Override
    protected WebElement elementForLoading() throws Exception {
        return null;
    }


}
