@Feature_Request
Feature: Request

  @High @Request @StaffingRequestCreationNike
  Scenario: Staffing request creationNike
    Given I`m on a home page
    Then I click on the Create request button;
    And I select "Alison Wright" from manager dropdown
    And I select the request type "Staffing"
    And I enter "Business Devt Associate" into job title field and select it from the list
    And I enter "2308 Crest Lane. Menlo Park. CA. 94025. US" into the Client location field and select it from the list
    And I select "Full time" radio button
    And I click on link Edit
    And I fill in field "Duties" with text "123123" on page Job Details
    And I fill in field "Skills" with text "123123" on page Job Details
    And I fill in field "Education" with text "123123" on page Job Details
    And I click on the Next button
    And I fill in "Start date" field with value "05/25/2021" on page Details
    And I fill in "End date" field with value "05/25/2030" on page Details
    And I fill in "Total Positions" field with value "5" on page Details
    And I fill in "Hours Per Week" field with value "40" on page Details
    And I select "Conversion" from Reason for hire dropdown on page Details
    And I fill in "Bill Rate" field with value "5" on page Details
    And I select department with name "CEOs Department", number "82A" and parent name "HR/Legal/SiteSvcs"
    And I fill in "GLCode" field with value "123123" on page Details
    And I click on the Next button
    And I click on the Next button
    And I enter "095095095" in test mobile field
    And I select "Yes Domestic" from "Travel Required" radio buttons
    And I select "Yes" from "Is this a GMP non-employee?" radio buttons
    And I select "Yes" from "Medical Exam Required" dropdown on 'Additional Information' page
    And I select "Yes" from "Worker requires access to InterMune systems" dropdown on 'Additional Information' page
    And I click on the Next button
    And I click on the Submit button
