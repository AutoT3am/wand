package testUtils;

import helpers.SystemHelper;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import pages.LoginPage;
import pages.base.PageInstance;

/**
 * Created by beckmambetov on 5/20/2016.
 */
public class Login extends PageInstance{


    @Lazy
    @Autowired
    public LoginPage loginPage;

    public void perform() {
        driver.findElement(By.xpath(".//input[@id='usernamefield']")).sendKeys(SystemHelper.USERNAME);
        driver.findElement(By.xpath(".//input[@id='passwordfield']")).sendKeys(SystemHelper.PASSWORD);
        driver.findElement(By.xpath(".//button[@name='loginButton']")).click();
    }
}
