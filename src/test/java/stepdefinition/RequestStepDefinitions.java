package stepdefinition;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import arp.CucumberArpReport;
import arp.ReportService;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import pages.CommonPage;
import pages.base.PageInstance;
import pages.page.*;

import static pages.base.PageInstance.checkIfFurtherStepsAreNeeded;
import static pages.base.PageInstance.driver;

/**
 * Created by beckmambetov on 5/20/2016.
 */
public class RequestStepDefinitions extends PageInstance {


    @Autowired
    public CreateRequestPage createRequestPage;
    @Autowired
    public ReviewAndSubmitPage reviewAndSubmitPage;
    @Autowired
    public AdditionalInformationPage additionalInformationPage;
    @Autowired
    public JobDescriptionPage jobDescriptionPage;
    @Autowired
    public DetailsPage detailsPage;

    @Given("^I`m on a home page$")
    public void I_m_on_a_home_page() throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            waitForJStoLoad();
            ReportService.ReportAction("I`m on a main page", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            e.printStackTrace();
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I click on the Create request button;$")
    public void I_click_on_the_Create_request_button() throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            waitForJStoLoad();
            createRequestPage.buttonCreateRequest.click();
            Thread.sleep(3000);
            ReportService.ReportAction("Create request button was clicked", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }


    @And("^I select \"([^\"]*)\" from manager dropdown$")
    public void I_select_from_manager_dropdown(String arg1) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            waitForJStoLoad();
            createRequestPage.selectFromManagerDropdown(arg1);
            ReportService.ReportAction("Manager type was selected", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I select the request type \"([^\"]*)\"$")
    public void I_select_the_request_type(String arg1) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            waitForJStoLoad();
            createRequestPage.selectRequestType(arg1);
            Thread.sleep(1000);
            ReportService.ReportAction("Request type was selected", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I enter \"([^\"]*)\" into job title field and select it from the list$")
    public void I_enter_into_job_title_field(String arg1) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            waitForJStoLoad();
            jobDescriptionPage.selectJob(arg1);
            ReportService.ReportAction("Job was selected", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click on the Submit button$")
    public void iClickOnTheSubmitButton() throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            waitForJStoLoad();
            reviewAndSubmitPage.clickOnSubmitButton();
            ReportService.ReportAction("Submit button was clicked", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click on the Next button$")
    public void iClickOnTheNextButton() throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            waitForJStoLoad();
            createRequestPage.buttonNext.click();
            Thread.sleep(2000);
            ReportService.ReportAction("Button Next was clicked", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }


    @And("^I enter \"([^\"]*)\" in test mobile field$")
    public void iEnterInTestMobileField(String phoneNumber) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            waitForJStoLoad();
            additionalInformationPage.enterTextToMobileTextInput(phoneNumber);
            ReportService.ReportAction("Text was entered", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }


    @And("^I select \"([^\"]*)\" from \"([^\"]*)\" radio buttons$")
    public void iSelectFromRadioButtons(String buttonName, String sectionName) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            waitForJStoLoad();
            additionalInformationPage.clickOnButton(buttonName, sectionName);
            ReportService.ReportAction("Radio button was selected", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }


    @And("^I enter \"([^\"]*)\" into the Client location field and select it from the list$")
    public void I_enter_into_the_Client_location_field_and_select_it_from_the_list(String arg1) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            waitForJStoLoad();
            jobDescriptionPage.selectLocation(arg1);
            ReportService.ReportAction("Location was chosen from list", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }


    @And("^I select \"([^\"]*)\" radio button$")
    public void I_select_radio_button(String arg1) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            waitForJStoLoad();
            jobDescriptionPage.selectJobTime(arg1);
            ReportService.ReportAction("Job time was chosen", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click on link Edit$")
    public void I_click_on_link_Edit() throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            waitForJStoLoad();
            jobDescriptionPage.openBoxesForEditing();
            ReportService.ReportAction("Edit link was clicked", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I fill in \"([^\"]*)\" field with value \"([^\"]*)\" on page Details$")
    public void I_fill_in_field_with_value(String arg1, String arg2) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            waitForJStoLoad();
            WebElement temp;
            switch (arg1.toLowerCase().replace(" ", "")) {
                case "startdate": {
                    temp = detailsPage.textInputStartDate;
                    break;
                }
                case "enddate": {
                    temp = detailsPage.textInputEndDate;
                    break;
                }
                case "totalpositions": {
                    temp = detailsPage.textInputPositions;
                    break;
                }
                case "hoursperweek": {
                    temp = detailsPage.textInputHoursPerWeek;
                    break;
                }
                case "billrate": {
                    temp = detailsPage.textInputBillRate;
                    break;
                }
                case "glcode": {
                    temp = detailsPage.textInputGLCode;
                    break;
                }
                default: {
                    throw new Exception("No such field");
                }
            }
            try {
                temp.click();
            } catch (WebDriverException ex) {
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", temp);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            temp.sendKeys(arg2);
            ReportService.ReportAction("Value was entered in a field", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }


    @And("^I select \"([^\"]*)\" from Reason for hire dropdown on page Details$")
    public void I_select_from_Reason_for_hire_dropdown_on_page_Details(String arg1) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            waitForJStoLoad();
            detailsPage.selectFromReasonDropdown(arg1);
            ReportService.ReportAction("Create request button was clicked", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }


    @And("^I select \"([^\"]*)\" from \"([^\"]*)\" dropdown on 'Additional Information' page$")
    public void iSelectFromDropdownOnAdditionalInformationPage(String value, String dropDownName) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            waitForJStoLoad();
            additionalInformationPage.selectFromDropDown(value, dropDownName);
            ReportService.ReportAction("Create request button was clicked", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }


    @And("^I select department with name \"([^\"]*)\", number \"([^\"]*)\" and parent name \"([^\"]*)\"$")
    public void I_select_department_with_name_number_and_parent_name(String arg1, String arg2, String arg3) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            waitForJStoLoad();
            detailsPage.selectDepartment(arg1, arg2, arg3);
            ReportService.ReportAction("Create request button was clicked", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }


    @And("^I fill in field \"([^\"]*)\" with text \"([^\"]*)\" on page Job Details$")
    public void I_fill_in_field_with_text_on_page_Job_Details(String arg1, String arg2) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            waitForJStoLoad();
            jobDescriptionPage.enterTextIntoBox(arg1.toLowerCase(), arg2);
            ReportService.ReportAction("Create request button was clicked", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
